<?php
/**
 * Created by PhpStorm.
 * User: Lukas
 * Date: 1.9.14
 * Time: 12:41
 */

namespace core\forms;


interface IFormRules {

    const RULE_REQUIRED = 0;
    const RULE_MAX_LENGTH = 1;

} 