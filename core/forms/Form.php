<?php
/**
 * Created by PhpStorm.
 * User: Lukas
 * Date: 1.9.14
 * Time: 11:06
 */

namespace core\forms;

use core\forms\TextInput,
    core\forms\Control;


class Form implements IFormRules {

    /**
     * POST/GET/PUT ...
     * @var string
     */
    private $method = 'POST';

    /**
     * @var string
     */
    private $encType = 'application/x-www-form-urlencoded';

    /**
     * action url
     * @var string
     */
    private $action = '';

    /**
     * @var Control[]
     */
    private $elements = array();

    /**
     * @param $method string
     */
    public function setMethod($method)
    {
        $this->method = $method;
    }

    /**
     * @param $type string
     */
    public function setEncType($type)
    {
        $this->encType = $type;
    }

    /**
     * @param string $url
     */
    public function setAction($url)
    {
        $this->action = $url;
    }

    /**
     * @param $name
     * @param $label
     * @return TextInput
     */
    public function addTextInput($name, $label)
    {
        $input = new TextInput($name, $label);
        $this->elements[$name] = $input;
        return $this->elements[$name];
    }

    /**
     * @param $name
     * @return SubmitButton
     */
    public function addSubmit($name)
    {
        $input = new SubmitButton($name);
        $this->elements[$name] = $input;
        return $this->elements[$name];
    }

    public function addSelect($name, array $values)
    {
        $select = new SelectInput($name);
        $select->setValues($values);
        $this->elements[$name] = $select;
        return $this->elements[$name];
    }



    /**
     * render html
     */
    public function renderAllInputs()
    {
        $htmlStorage = array();
        foreach($this->elements as $element)
        {
            $htmlStorage[] = $element->render();
        }
        $builder = new HtmlBuilder();
        $builder->generatePairElement('form', array('action' => $this->action, 'method' => $this->method, 'enctype' => $this->encType), $htmlStorage);
        echo $builder->render();
    }

    /**
     * Validate form after submit
     */
    public function validate()
    {
        foreach($this->elements as $element)
        {
            $element->validate();
        }
    }

    public function getElements()
    {
        print_r($this->elements);
    }

} 