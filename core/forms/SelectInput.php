<?php
/**
 * Created by PhpStorm.
 * User: Lukas
 * Date: 1.9.14
 * Time: 13:24
 */

namespace core\forms;


class SelectInput extends Control {

    private $values;

    public function setValues(array $values)
    {
        $builder = new HtmlBuilder();
        foreach($values as $key => $value)
        {
            $this->values = $builder->generatePairElement('option', array('value' => $key), $value)->render();
        }
    }

    public function render()
    {
        $builder = new HtmlBuilder();

        // generate label
        $builder->generatePairElement('select', array('for' => $this->params['name']), $this->values);

        return $builder->render();
    }

} 