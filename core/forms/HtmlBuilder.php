<?php
/**
 * Created by PhpStorm.
 * User: Lukas
 * Date: 31.8.14
 * Time: 22:31
 */

namespace core\forms;


class HtmlBuilder {

    /**
     * @var string
     */
    private $html = '';

    public function __construct()
    {
    }

    /**
     * Generate Pair Element
     * @param $type
     * @param array $params
     * @param $value
     * @return $this
     */
    public function generatePairElement($type, $params = array(), $value)
    {
        $this->html .= '<' . htmlspecialchars($type);
        foreach($params as $key => $val)
        {
            $this->html .= ' ' . htmlspecialchars($key) . '="' . htmlspecialchars($val) . '"';
        }
        $this->html .= ' >';
        if(is_array($value))
        {
            foreach($value as $element)
            {
                $this->html .= $element;
            }
        }
        else
        {
            $this->html .= $value;
        }
        $this->html .= '</' . $type . '>';
        return $this;
    }

    /**
     * Generate non pair element
     * @param $type
     * @param array $params
     */
    public function generateNonPairElement($type, $params = array())
    {
        $this->html .= '<' . htmlspecialchars($type);
        foreach($params as $key => $value)
        {
            $this->html .= ' ' . htmlspecialchars($key) . '="' . htmlspecialchars($value) . '"';
        }
        $this->html .= ' />';
    }

    public function render()
    {
        return $this->html;
    }


} 