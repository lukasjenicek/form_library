<?php
/**
 * Created by PhpStorm.
 * User: Lukas
 * Date: 1.9.14
 * Time: 13:04
 */

namespace core\forms;


class SubmitButton extends Control {

    public function render()
    {
        $builder = new HtmlBuilder();
        //
        $this->params['type'] = 'submit';
        // generate submit button
        $builder->generateNonPairElement('input', $this->params);
        return $builder->render();
    }

} 