<?php
/**
 * Created by PhpStorm.
 * User: Lukas
 * Date: 31.8.14
 * Time: 22:16
 */

namespace core\forms;


class TextInput extends Control {

    public function render()
    {
        $builder = new HtmlBuilder();

        // generate label
        $builder->generatePairElement('label', array('for' => $this->params['name']), $this->params['label']);

        // unset label param we dont want to be this parameter dumped like a input parameter
        // it would be invalid markup
        if(isset($this->params['label'])) { unset($this->params['label']); }

        // generate input
        $builder->generateNonPairElement('input', $this->params);
        return $builder->render();
    }

} 