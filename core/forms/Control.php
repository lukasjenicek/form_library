<?php
/**
 * Created by PhpStorm.
 * User: Lukas
 * Date: 31.8.14
 * Time: 22:14
 */

namespace core\forms;


abstract class Control implements IFormRules {

    /**
     * html parameters
     * @var array
     */
    protected $params;

    /**
     * @var array
     */
    protected $rules = array();

    /**
     * @param string $name (input name)
     * @param string $label (label name)
     */
    public function __construct($name, $label = null)
    {
        $this->params = array('name' => $name, 'id' => $name);
        if(isset($label)) { $this->params['label'] = $label ; }
    }

    /**
     * @param string $key Attribute Name
     * @param string $value Value of Attribute
     * @return $this
     */
    public function setAttribute($key, $value)
    {
        // if attribute exist
        if(isset($this->params[$key]))
        {
            $this->params[$key] .= ' ' . $value;
        }
        else
        {
            $this->params[$key] = $value;
        }
        return $this;
    }

    private function addRuleToArray($rule)
    {
        $this->rules[] = $rule;
        return $this;
    }

    public function setRequired($msg = 'Toto políčko je povinné')
    {
        return $this->addRuleToArray(array(
            'type' => self::RULE_REQUIRED,
            'message' => $msg
        ));
    }

    public function addRule($rule, $msg)
    {
        return $this->addRuleToArray(array(
            'type' => $rule,
            'message' => $msg
        ));
    }

    /**
     * @param $rule
     * @return bool
     */
    private function checkRule($rule)
    {
        switch($rule['type'])
        {
            case self::RULE_REQUIRED:
                if(!isset($_POST[$this->params['name']]) || empty($_POST[$this->params['name']]))
                {
                    return false;
                }
                break;
            case self::RULE_MAX_LENGTH:
                if(trim(strlen($_POST[$this->params['name']])))
                {
                    return false;
                }
                break;
        }
        return true;
    }

    /**
     * @throws FormException
     */
    public function validate()
    {
        foreach($this->rules as $rule)
        {
            if(!$this->checkRule($rule))
            {
                throw new FormException($rule['message']);
            }
        }
    }

    abstract public function render();

} 