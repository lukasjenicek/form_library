<?php
/**
 * Created by PhpStorm.
 * User: Lukas
 * Date: 1.9.14
 * Time: 13:43
 */

require_once 'vendor/autoload.php';

$form = new \core\forms\Form();

$form->addTextInput('username', 'Username: ')
    ->setAttribute('class', 'input_class')
    ->setAttribute('title', 'Tool tip options')
    ->setRequired();

$form->addTextInput('email', 'Email: ')
    ->addRule($form::RULE_MAX_LENGTH, 'Toto políčko může mít maximálně 20 charů :D')
    ->setRequired();

$form->addSelect('cars', array(
    'audi' => 'Audi',
    'bmw' => 'BWM',
    'Peugeot' => 'Peugeot'
));

$form->addSubmit('contact-form')
    ->setAttribute('value', 'Odeslat kontaktní formulář');


$form->setAction('/Jenda_Framework/contact');
$form->setMethod('POST');


$form->renderAllInputs();